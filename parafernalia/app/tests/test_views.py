import json
from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from parafernalia.app.models import FlightModel


class FlightCreateTest(APITestCase):
    """Test View Demands."""
    def setUp(self):
        pass

    def test_create_demand_success(self):
        """Ensure we can create a new flight."""
        url = reverse('flights:get_post_handler')
        data = {
            "origin": "Rio de Janeiro",
            "to": "Sao Paulo",
            "date": "2019-10-05T16:02:24.428808",
            "company": "Latam"
        }

        response = self.client.post(
            path=url,
            data=json.dumps(data),
            content_type='Application/json',
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(FlightModel.objects.count(), 1)
        self.assertEqual(FlightModel.objects.get().origin, 'Rio de Janeiro')

    def test_create_flight_missing_field(self):
        """Ensure we can't create a new flight with missing field."""
        url = reverse('flights:get_post_handler')
        data = {
            "from": "Rio de Janeiro",
            "to": "Sao Paulo",
            "date": "2019-10-05T16:02:24.428808",
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class FlightGetTest(APITestCase):
    def setUp(self):
        self.url = reverse('flights:get_post_handler')
        self.data = {
            "origin": "Rio de Janeiro",
            "to": "Sao Paulo",
            "date": "2019-10-05T16:02:24.428808",
            "company": "Latam"
        }

    def test_get_flight_with_origin(self):
        """Test get flight with origin."""
        self.client.post(
            path=self.url,
            data=json.dumps(self.data),
            content_type='Application/json',
        )
        response = self.client.get(self.url, {'from': 'Rio de Janeiro'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_flight_with_origin_to(self):
        """Test get flight with origin and destiny."""
        self.client.post(
            path=self.url,
            data=json.dumps(self.data),
            content_type='Application/json',
        )
        response = self.client.get(self.url, {
            'from': 'Rio de Janeiro',
            'to': 'Sao Paulo'
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_flight_with_origin_date(self):
        """Test get flight with origin and date."""
        self.client.post(
            path=self.url,
            data=json.dumps(self.data),
            content_type='Application/json',
        )
        response = self.client.get(self.url, {
            'from': 'Rio de Janeiro',
            'departure': '2019-10-05'
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_flight_with_to(self):
        """Test get flight with destiny."""
        self.client.post(
            path=self.url,
            data=json.dumps(self.data),
            content_type='Application/json',
        )
        response = self.client.get(self.url, {'to': 'Sao Paulo'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_flight_with_to_date(self):
        """Test get flight with destiny and date."""
        self.client.post(
            path=self.url,
            data=json.dumps(self.data),
            content_type='Application/json',
        )
        response = self.client.get(self.url, {
            'to': 'Sao Paulo',
            'departure': '2019-10-05'
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_flight_with_date(self):
        """Test get flight with date."""
        self.client.post(
            path=self.url,
            data=json.dumps(self.data),
            content_type='Application/json',
        )
        response = self.client.get(self.url, {'departure': '2019-10-05'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_flight_with_all_params(self):
        """Test get flight with all params."""
        self.client.post(
            path=self.url,
            data=json.dumps(self.data),
            content_type='Application/json',
        )
        response = self.client.get(self.url, {
            'from': 'Rio de Janeiro',
            'to': 'Sao Paulo',
            'departure': '2019-10-05'
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_flight_no_content_origin(self):
        """Test flight origin not found."""
        self.client.post(
            path=self.url,
            data=json.dumps(self.data),
            content_type='Application/json',
        )
        response = self.client.get(self.url, {'from': 'Sao Paulo'})
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_get_flight_no_content_destiny(self):
        """Test flight destiny not found."""
        self.client.post(
            path=self.url,
            data=json.dumps(self.data),
            content_type='Application/json',
        )
        response = self.client.get(self.url, {'to': 'Porto Alegre'})
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_get_flight_no_content_date(self):
        """Test flight date not found."""
        self.client.post(
            path=self.url,
            data=json.dumps(self.data),
            content_type='Application/json',
        )
        response = self.client.get(self.url, {'departure': '2019-01-01'})
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)