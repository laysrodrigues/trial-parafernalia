from django.urls import path

from . import views as flights_views

app_name = 'app'

urlpatterns = [
    path(
        '',
        flights_views.get_post_handler,
        name='get_post_handler',
    ),
]
