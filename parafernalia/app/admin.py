from django.contrib import admin
from .models import FlightModel

# Register your models here.


class FlightModelAdmin(admin.ModelAdmin):
    list_display = (
        'origin',
        'to',
        'date',
        'company',
    )


admin.site.register(FlightModel, FlightModelAdmin)