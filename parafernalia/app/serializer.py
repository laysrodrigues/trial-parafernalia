from rest_framework import serializers
from .models import FlightModel


class FlightSerializer(serializers.ModelSerializer):
    """Serializer for Flights Model."""
    class Meta:
        model = FlightModel
        fields = '__all__'