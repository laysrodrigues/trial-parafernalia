from django.db import transaction
from django.shortcuts import render
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from .models import FlightModel
from .serializer import FlightSerializer


@api_view(['POST', 'GET'])
def get_post_handler(request):
    """Get flight or create a new flight."""
    if request.method == 'POST':
        return create(request)
    return get(request)


@transaction.atomic
def create(request):
    """Create a new flight."""
    data = JSONParser().parse(request)
    serializer = FlightSerializer(data=data)
    if serializer.is_valid():
        serializer.create(validated_data=serializer.data)
        return Response(status=status.HTTP_201_CREATED)
    return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)


def get(request):
    """Get flights of a given filter."""
    origin = request.GET.get('from', None)
    to = request.GET.get('to', None)
    departure = request.GET.get('departure', None)

    filters = {}
    if origin:
        filters['origin'] = origin
    if to:
        filters['to'] = to
    if departure:
        filters['date__date'] = departure

    query = FlightModel.objects.filter(**filters)

    if query.count() > 0:
        flights = FlightSerializer(query, many=True)
        if flights:
            return Response(data=flights.data, status=status.HTTP_200_OK)
    else:
        return Response(status=status.HTTP_204_NO_CONTENT)
    return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
