import datetime
import os
import rows
from django.db import transaction
from parafernalia.app.models import FlightModel
from django_tqdm import BaseCommand
from tqdm import tqdm


class Command(BaseCommand):
    help = 'Load a csv file into the dabatase as models.'

    @transaction.atomic
    def handle(self, *args, **kargs):
        self.info('Starting the parse of csv')
        entries = rows.import_from_csv((os.path.join(
            os.getcwd(),
            'parafernalia/app/management/commands/data/BrFlights2.csv')),
                                       encoding='ISO-8859-15')
        self.info('Starting the import to the db.')
        t = self.tqdm(len(entries))
        print(entries.fields)
        for entry in entries:
            ok = FlightModel.objects.create(origin=(entry.cidadeorigem),
                                            to=(entry.cidadedestino),
                                            date=(parse_date(
                                                entry.partidaprevista)),
                                            company=(entry.companhiaaerea))
            t.update(1)
            if not ok:
                print('Error on adding flight to db.')


def parse_date(date_string):
    return datetime.datetime.strptime(date_string, '%Y-%m-%dT%H:%M:%S%z')
