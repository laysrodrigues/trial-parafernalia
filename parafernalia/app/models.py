from django.db import models

# Create your models here.


class FlightModel(models.Model):

    origin = models.CharField(verbose_name='From', max_length=200)
    to = models.CharField(verbose_name='To', max_length=200)
    date = models.DateTimeField(verbose_name='Date')
    company = models.CharField(verbose_name='Flight Company', max_length=100)

    class Meta:
        verbose_name = 'Flight'
        verbose_name_plural = 'Flights'