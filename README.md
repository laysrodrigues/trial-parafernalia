# Trial Parafernalia

[![pipeline status](https://invent.kde.org/laysrodrigues/trial-parafernalia/badges/master/pipeline.svg)](https://invent.kde.org/laysrodrigues/trial-parafernalia/commits/master)
[![coverage report](https://invent.kde.org/laysrodrigues/trial-parafernalia/badges/master/coverage.svg)](https://invent.kde.org/laysrodrigues/trial-parafernalia/commits/master)

## How to Run

### Requirements

1) Clone this repository
2) Create your virtualenv with Python 3.7

#### For Development

1) Install this app
    `pip install -e .`
2) Configure your local environment vars based on the file `.env.example` and run:
    `source .env`

## Loading CSV Data

To load the CSV data please execute the following command:

```sh
python manage.py load_data_to_db.py
```

## Time Spent

### For the API

For the development of the API I have spent around 2 hours between coding, thinking and researching.

### For the script

For the script to load the CSV I have spent around 1 hour, between coding, thinking and researching.
