from setuptools import setup, find_packages

setup(
    name='droids_quotation_api',
    author='Lays Rodrigues',
    author_email='lays.rodrigues@kde.org',
    description='Parafernalia Trial, an API for flight searcher',
    packages=find_packages(exclude=('tests')),
    install_requires=[
        'coverage==4.5.4',
        'django-nose==1.4.6',
        'Django==2.2.6',
        'djangorestframework==3.10.3',
        'dj-database-url==0.5.0',
        'psycopg2==2.8.3',
    ],
    extras_require={
        'dev': [
            'yapf==0.28.0',
            'pylint==2.4.2',
        ],
    },
)
